package persistence.entities

import java.util.UUID

case class User(id : String,
                    sureName: String,
                    firstName: String,
                    gender: String,
                    email: String,
                    subscribedNewsletter: Boolean) {
  def toSimpleUser = SimpleUser(this.sureName,this.firstName, this.gender, this.email, this.subscribedNewsletter)
}

case class SimpleUser(sureName: String,
                          firstName: String,
                          gender: String,
                          email: String,
                          subscribedNewslater: Boolean){
  def toUser = User(UUID.randomUUID().toString,
    this.sureName,
    this.firstName,
    this.gender,
    this.email,
    this.subscribedNewslater)
}