package persistence

import persistence.entities.SimpleUser
import spray.json.DefaultJsonProtocol

object JsonProtocol extends DefaultJsonProtocol {
  implicit val userFormat = jsonFormat5(SimpleUser)
}