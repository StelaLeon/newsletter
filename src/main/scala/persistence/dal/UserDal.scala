package persistence.dal

import persistence.entities.User
import io.getquill._

import scala.concurrent.{Awaitable, ExecutionContext, Future}

trait UserDal {
  def insert(userToInsert: User)(implicit ec: ExecutionContext): Future[Long]
  def findById(userId: String)(implicit ec: ExecutionContext) : Future[Option[User]]
  def delete(userId: String)(implicit ec: ExecutionContext): Future[Long]
}

class UserDalImpl(context: PostgresAsyncContext[SnakeCase]) extends UserDal {

  import context._

  override def insert(userToInsert: User)(implicit ec: ExecutionContext): Future[Long] = {
    context.run(query[User].insert)(userToInsert :: Nil).map(_.head)
  }

  override def findById(userId: String)(implicit ec: ExecutionContext) : Future[Option[User]] = {
    val q = quote {
      query[User].filter(_.id == lift(userId))
    }

    println("query: "+q.toString)
    context.run(q).map(_.headOption)
  }

  /**override def findAll()(implicit ec: ExecutionContext) : Future[Option[List[User]]] = {
    val q = quote {
      query[User]
    }
    context.run(q)
  }**/

  override def delete(userId: String)(implicit ec: ExecutionContext): Future[Long] = {
    val q = quote {
      query[User].filter(_.id == lift(userId)).delete
    }
    context.run(q)
  }
}
