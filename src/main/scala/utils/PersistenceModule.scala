package utils

import io.getquill.{PostgresAsyncContext, SnakeCase}
import persistence.dal.{UserDal, UserDalImpl}

trait 	DbContext {
	val context : PostgresAsyncContext[SnakeCase]
}

trait PersistenceModule {
  val userDal : UserDalImpl
}


trait PersistenceModuleImpl extends PersistenceModule with DbContext{
	this: Configuration  =>

	override lazy val context = new PostgresAsyncContext[SnakeCase]("quill")
	override val userDal = new UserDalImpl(context)

}
