package rest

import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.{Directives, Route}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import persistence.JsonProtocol
import JsonProtocol._
import SprayJsonSupport._
import utils.{Configuration, PersistenceModule}

import scala.util.{Failure, Success}
import io.swagger.annotations._
import javax.ws.rs.Path

import persistence.entities.{User,SimpleUser}

import scala.concurrent.ExecutionContext

@Path("/user")
@Api(value = "/user", produces = "application/json")
class UserRoutes(modules: Configuration with PersistenceModule)(implicit ec: ExecutionContext)  extends Directives {

  @Path("/{id}")
  @ApiOperation(value = "Return User", httpMethod = "GET")
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "id", value = "User Id", required = false, dataType = "int", paramType = "path")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 200, message = "Return User", response = classOf[User]),
    new ApiResponse(code = 400, message = "The User id should be greater than zero"),
    new ApiResponse(code = 404, message = "Return User Not Found"),
    new ApiResponse(code = 500, message = "Internal server error")
  ))
  def userGetRoute = path("user" / IntNumber.?) { (supId) =>
    get {
        println("got a get request from the user path/id")
        onComplete((modules.userDal.findById(supId.toString)).mapTo[Option[User]]) {
          case Success(supplierOpt) => supplierOpt match {
            case Some(sup) => complete(sup.toSimpleUser)
            case None => complete(NotFound, s"The supplier doesn't exist")
          }
          case Failure(ex) => complete(InternalServerError, s"An error occurred: ${ex.getMessage}")
        }
      }
  }
  def usersGetAllRoute = path("allUsers") {
    get {
      println("got a get request from the user path/id")
      complete("All users")
      /**onComplete((modules.userDal.findById(supId.toString)).mapTo[Option[User]]) {
        case Success(supplierOpt) => supplierOpt match {
          case Some(sup) => complete("All users")
          case None => complete(NotFound, s"The supplier doesn't exist")
        }
        case Failure(ex) => complete(InternalServerError, s"An error occurred: ${ex.getMessage}")
      }**/
    }
  }

  val routes: Route =  userGetRoute ~ usersGetAllRoute


  //@TODO get all users path
}

