import akka.http.scaladsl.Http
import akka.http.scaladsl.server.RouteConcatenation
import akka.stream.ActorMaterializer
import utils._
import rest.UserRoutes

object Main extends App with RouteConcatenation {
  // configuring modules for application, cake pattern for DI
  val modules = new ConfigurationModuleImpl  with ActorModuleImpl with PersistenceModuleImpl
  implicit val system = modules.system
  implicit val materializer = ActorMaterializer()
  implicit val ec = modules.system.dispatcher

  val swaggerService = new SwaggerDocService(system)

  val bindingFuture = Http().bindAndHandle(
    new UserRoutes(modules).routes ~
    swaggerService.assets , "0.0.0.0", 8080)

  println(s"Server online at http://localhost:8080/")

}